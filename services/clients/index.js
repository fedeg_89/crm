const app = require ('./src/server/server')

const PORT = process.env.PORT

app.listen(PORT, () => {
	console.log(`Clients Service listening on port ${PORT}`)
})
