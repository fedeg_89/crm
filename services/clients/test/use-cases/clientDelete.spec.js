const ClientDelete = require('../../src/use-cases/ClientDelete')
const dbErrorHandler = require('../../src/errors/dbErrorHandler')
// SQL
const SqlDb = require('../../src/interfaces/SQLAdapter')
const sqlConnection = require('../../src/drivers/alasql')
let sqlDb = new SqlDb({ dbConnection: sqlConnection, dbErrorHandler })

// NoSQL
const NoSqlDb = require('../../src/interfaces/NoSQLAdapter')
const nosqlConnection = require('../../src/drivers/lokidb')
let nosqlDb = new NoSqlDb({ dbConnection: nosqlConnection, dbErrorHandler })

sqlClientDelete = new ClientDelete({ db: sqlDb })
nosqlClientDelete = new ClientDelete({ db: nosqlDb })

// const Client = require('../../src/entities/Client')
const ClientError = require('../../src/errors/ClientError')
const DatabaseError = require('../../src/errors/DatabaseError')

const { client1, client2 } = require('../fake-data/clientData.json')
let fakeClient1, fakeClient2
const statusOk = { status: 'ok' } 

async function resetDatabases() {
	await sqlConnection.exec('DELETE FROM clients')
	await sqlConnection.exec('DELETE FROM agents')
	await nosqlConnection.removeCollection('clients')
	await nosqlConnection.removeCollection('agents')
	await nosqlConnection.addCollection('clients', {
		indices: ['id'],
		unique: ['name', 'domain']
	})

	await nosqlConnection.addCollection('agents', {
		indices: ['id'],
	})

	
	Promise.resolve()
}

describe('Client Delete', () => {
	beforeEach(async () => {
		fakeClient1 = { ...client1 }
		fakeClient2 = { ...client2 }
		await resetDatabases()
	})

	describe('delete a client', () => {
		test('sql', async () => {
			// Insert base records
			await sqlConnection.exec(`INSERT INTO clients VALUES ?`, [fakeClient1])
			await sqlConnection.exec(`INSERT INTO clients VALUES ?`, [fakeClient2])
			const initialDb = await sqlConnection.exec(`SELECT * FROM clients`)
			expect(initialDb).toHaveLength(2)

			// Delete by id
			const deleteResult1 = await sqlClientDelete.run(fakeClient1.id)
			expect(deleteResult1).toMatchObject(statusOk)
			expect(deleteResult1.message).toEqual(expect.stringMatching(fakeClient1.id))

			// Delete by object
			const deleteResult2 = await sqlClientDelete.run(fakeClient2)
			expect(deleteResult2).toMatchObject(statusOk)
			expect(deleteResult2.message).toEqual(expect.stringMatching(fakeClient2.id))

			const finalDb = await sqlConnection.exec(`SELECT * FROM clients`)
			expect(finalDb).toHaveLength(0)
		})

		test('nosql', async () => {
			// Insert base records
			await nosqlConnection.getCollection('clients').insert(fakeClient1)
			await nosqlConnection.getCollection('clients').insert(fakeClient2)
			const initialDb = nosqlConnection.getCollection('clients').find()
			expect(initialDb).toHaveLength(2)

			// Delete by id
			const deleteResult1 = await nosqlClientDelete.run(fakeClient1.id)
			expect(deleteResult1).toMatchObject(statusOk)
			expect(deleteResult1.message).toEqual(expect.stringMatching(fakeClient1.id))

			// Delete by object
			const deleteResult2 = await nosqlClientDelete.run(fakeClient2)
			expect(deleteResult2).toMatchObject(statusOk)
			expect(deleteResult2.message).toEqual(expect.stringMatching(fakeClient2.id))

			const finalDb = nosqlConnection.getCollection('clients').find()
			expect(finalDb).toHaveLength(0)
		})
	})

	describe('errors deleting new client', () => {
		beforeEach(async () => {
			fakeClient1 = { ...client1 }
			fakeClient2 = { ...client2 }
			await resetDatabases()
		})

		describe('record not found', () => {
			test('sql', async () => {
				// Insert base records
				await sqlConnection.exec(`INSERT INTO clients VALUES ?`, [fakeClient1])
				await sqlConnection.exec(`INSERT INTO clients VALUES ?`, [fakeClient2])

				fakeClient1.id = '123a123'

				async function createClient(fakeData) {
					await sqlClientDelete.run(fakeData)
				}
				// Delete by id
				await expect(createClient(fakeClient1.id)).rejects.toThrowError('Record not found')
				await expect(createClient(fakeClient1.id)).rejects.toThrowError(DatabaseError)

				// Delete by object
				await expect(createClient(fakeClient1)).rejects.toThrowError('Record not found')
				await expect(createClient(fakeClient1)).rejects.toThrowError(DatabaseError)

				const finalDb = await sqlConnection.exec(`SELECT * FROM clients`)
				expect(finalDb).toHaveLength(2)
			})

			test('nosql', async () => {
				// Insert base records
				await nosqlConnection.getCollection('clients').insert(fakeClient1)
				await nosqlConnection.getCollection('clients').insert(fakeClient2)
				fakeClient1.id = '123a123'

				async function createClient(fakeData) {
					await nosqlClientDelete.run(fakeData)
				}
				// Delete by id
				await expect(createClient(fakeClient1.id)).rejects.toThrowError('Record not found')
				await expect(createClient(fakeClient1.id)).rejects.toThrowError(DatabaseError)

				// Delete by object
				await expect(createClient(fakeClient1)).rejects.toThrowError('Record not found')
				await expect(createClient(fakeClient1)).rejects.toThrowError(DatabaseError)

				const finalDb = await nosqlConnection.getCollection('clients').find()
				expect(finalDb).toHaveLength(2)
			})
		})
	})

	describe('Use case validations', () => {
		beforeEach(async () => {
			fakeClient1 = { ...client1 }
			fakeClient2 = { ...client2 }
			await resetDatabases()
		})

		describe('throws error for id', () => {
			test('sql', async () => {
				delete fakeClient1.id
				async function createClient(fakeData) {
					await sqlClientDelete.run(fakeData)
				}
				// Client with no ID
				await expect(createClient(fakeClient1)).rejects.toThrowError('No ID provided')
				await expect(createClient(fakeClient1)).rejects.toThrowError(ClientError)

				// No params
				await expect(createClient()).rejects.toThrowError('Neither')
				await expect(createClient()).rejects.toThrowError(ClientError)
			})

			test('nosql', async () => {
				delete fakeClient1.id
				async function createClient(fakeData) {
					await nosqlClientDelete.run(fakeData)
				}
				// Client with no ID
				await expect(createClient(fakeClient1)).rejects.toThrowError('No ID provided')
				await expect(createClient(fakeClient1)).rejects.toThrowError(ClientError)

				// No params
				await expect(createClient()).rejects.toThrowError('Neither')
				await expect(createClient()).rejects.toThrowError(ClientError)
			})
		})

	})
})