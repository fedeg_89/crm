const ClientGet = require('../../src/use-cases/ClientGet')
const dbErrorHandler = require('../../src/errors/dbErrorHandler')
// SQL
const SqlDb = require('../../src/interfaces/SQLAdapter')
const sqlConnection = require('../../src/drivers/alasql')
let sqlDb = new SqlDb({ dbConnection: sqlConnection, dbErrorHandler })

// NoSQL
const NoSqlDb = require('../../src/interfaces/NoSQLAdapter')
const nosqlConnection = require('../../src/drivers/lokidb')
let nosqlDb = new NoSqlDb({ dbConnection: nosqlConnection, dbErrorHandler })

sqlClientGet = new ClientGet({ db: sqlDb })
nosqlClientGet = new ClientGet({ db: nosqlDb })

// const Client = require('../../src/entities/Client')
const ClientError = require('../../src/errors/ClientError')
const DatabaseError = require('../../src/errors/DatabaseError')

const { client1, client2 } = require('../fake-data/clientData.json')
let fakeClient1, fakeClient2
const statusOk = { status: 'ok' } 

async function resetDatabases() {
	await sqlConnection.exec('DELETE FROM clients')
	await sqlConnection.exec('DELETE FROM agents')
	await nosqlConnection.removeCollection('clients')
	await nosqlConnection.removeCollection('agents')
	await nosqlConnection.addCollection('clients', {
		indices: ['id'],
		unique: ['name', 'domain']
	})

	await nosqlConnection.addCollection('agents', {
		indices: ['id'],
	})

	
	Promise.resolve()
}

describe('Client Get', () => {
	beforeEach(async () => {
		fakeClient1 = { ...client1 }
		fakeClient2 = { ...client2 }
		await resetDatabases()
	})

	describe('get clients', () => {
		describe('sql', () => {
			test('by id', async () => {

				// Insert base records
				await sqlConnection.exec(`INSERT INTO clients VALUES ?`, [fakeClient1])
				await sqlConnection.exec(`INSERT INTO clients VALUES ?`, [fakeClient2])
			
				// Get one by id
				const getResult1 = await sqlClientGet.run(fakeClient1.id)
				expect(getResult1).toMatchObject(statusOk)
				expect(getResult1.message).toHaveLength(1)
				expect(getResult1.message[0].id).toEqual(expect.stringMatching(fakeClient1.id))
	
				// Get all
				const getResult2 = await sqlClientGet.run()
				expect(getResult2).toMatchObject(statusOk)
				expect(getResult2.message).toHaveLength(2)
			})
		})

		test('nosql', async () => {
			// Insert base records
			await nosqlConnection.getCollection('clients').insert(fakeClient1)
			await nosqlConnection.getCollection('clients').insert(fakeClient2)
			
			// Get one by id
			const getResult1 = await nosqlClientGet.run(fakeClient1.id)
			expect(getResult1).toMatchObject(statusOk)
			expect(getResult1.message).toHaveLength(1)
			expect(getResult1.message[0].id).toEqual(expect.stringMatching(fakeClient1.id))

			// Get all
			const getResult2 = await nosqlClientGet.run()
			expect(getResult2).toMatchObject(statusOk)
			expect(getResult2.message).toHaveLength(2)
		})
	})

	describe('errors getting clients', () => {
		beforeEach(async () => {
			fakeClient1 = { ...client1 }
			fakeClient2 = { ...client2 }
			await resetDatabases()
		})

		describe('record not found', () => {
			test('sql', async () => {
				// Insert base records
				await sqlConnection.exec(`INSERT INTO clients VALUES ?`, [fakeClient1])

				const fakeId = '123a123'

				async function createClient(fakeData) {
					await sqlClientGet.run(fakeData)
				}

				// Get fake id
				await expect(createClient(fakeId)).rejects.toThrowError('Record not found')
				await expect(createClient(fakeId)).rejects.toThrowError(DatabaseError)
			})

			test('nosql', async () => {
				// Insert base records
				await nosqlConnection.getCollection('clients').insert(fakeClient1)
				
				const fakeId = '123a123'

				async function createClient(fakeData) {
					await nosqlClientGet.run(fakeData)
				}

				// Get fake id
				await expect(createClient(fakeId)).rejects.toThrowError('Record not found')
				await expect(createClient(fakeId)).rejects.toThrowError(DatabaseError)
			})
		})
	})

	describe('Use case validations', () => {
		beforeEach(async () => {
			fakeClient1 = { ...client1 }
			fakeClient2 = { ...client2 }
			await resetDatabases()
		})

		describe('invalid id', () => {
			const badId1 = { id: '102b6739-22e7-46f1-8f27-ea57293da77c'}
			const badId2 = ['102b6739-22e7-46f1-8f27-ea57293da77c']
			const badId3 = 1234567890

			test('sql', async () => {
				async function createClient(fakeData) {
					await sqlClientGet.run(fakeData)
				}
				
				await expect(createClient(badId1)).rejects.toThrowError('Invalid')
				await expect(createClient(badId1)).rejects.toThrowError(ClientError)

				await expect(createClient(badId2)).rejects.toThrowError('Invalid')
				await expect(createClient(badId2)).rejects.toThrowError(ClientError)
				
				await expect(createClient(badId3)).rejects.toThrowError('Invalid')
				await expect(createClient(badId3)).rejects.toThrowError(ClientError)


			})

			test('nosql', async () => {
				async function createClient(fakeData) {
					await nosqlClientGet.run(fakeData)
				}

				await expect(createClient(badId1)).rejects.toThrowError('Invalid')
				await expect(createClient(badId1)).rejects.toThrowError(ClientError)

				await expect(createClient(badId2)).rejects.toThrowError('Invalid')
				await expect(createClient(badId2)).rejects.toThrowError(ClientError)
				
				await expect(createClient(badId3)).rejects.toThrowError('Invalid')
				await expect(createClient(badId3)).rejects.toThrowError(ClientError)
			})
		})

	})
})