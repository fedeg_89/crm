const ClientUpdate = require('../../src/use-cases/ClientUpdate')
const dbErrorHandler = require('../../src/errors/dbErrorHandler')
// SQL
const SqlDb = require('../../src/interfaces/SQLAdapter')
const sqlConnection = require('../../src/drivers/alasql')
let sqlDb = new SqlDb({ dbConnection: sqlConnection, dbErrorHandler })

// NoSQL
const NoSqlDb = require('../../src/interfaces/NoSQLAdapter')
const nosqlConnection = require('../../src/drivers/lokidb')
let nosqlDb = new NoSqlDb({ dbConnection: nosqlConnection, dbErrorHandler })

// Helpers
const isEmail = require('../../src/helpers/isEmail')
const isTimeFormat = require('../../src/helpers/isTimeFormat')
const isWeb = require('../../src/helpers/isWeb')

const helpers = {
	isEmail,
	isTimeFormat,
	isWeb
}

sqlClientUpdate = new ClientUpdate({ db: sqlDb, helpers })
nosqlClientUpdate = new ClientUpdate({ db: nosqlDb, helpers })

// const Client = require('../../src/entities/Client')
const ClientError = require('../../src/errors/ClientError')
const DatabaseError = require('../../src/errors/DatabaseError')

const { client1, client2 } = require('../fake-data/clientData.json')
let fakeClient1, fakeClient2
const statusOk = { status: 'ok' } 

async function resetDatabases() {
	await sqlConnection.exec('DELETE FROM clients')
	await sqlConnection.exec('DELETE FROM agents')
	await nosqlConnection.removeCollection('clients')
	await nosqlConnection.removeCollection('agents')
	await nosqlConnection.addCollection('clients', {
		indices: ['id'],
		unique: ['name', 'domain']
	})

	await nosqlConnection.addCollection('agents', {
		indices: ['id'],
	})

	
	Promise.resolve()
}

describe('Client Update', () => {
	beforeEach(async () => {
		fakeClient1 = { ...client1 }
		fakeClient2 = { ...client2 }
		await resetDatabases()
	})

	describe('update a client', () => {
		test('sql', async () => {
			// Insert base records
			await sqlConnection.exec(`INSERT INTO clients VALUES ?`, [fakeClient1])
			await sqlConnection.exec(`INSERT INTO clients VALUES ?`, [fakeClient2])

			const updatedClient1 = { ...fakeClient1 }
			updatedClient1.name = 'updated name'
			// Update
			const updateResult1 = await sqlClientUpdate.run(updatedClient1)
			expect(updateResult1).toMatchObject(statusOk)
			expect(updateResult1.message).toEqual(expect.stringMatching(updatedClient1.id))

			const clientUpdated1 = await sqlConnection.exec(`SELECT name FROM clients WHERE id = '${updatedClient1.id}'`)[0]
			expect(clientUpdated1.name).toBe(updatedClient1.name)

			const updatedClient2 = { ...fakeClient2 }
			updatedClient2.domain = 'updatedDomain'
			updatedClient2.closing_time = '19:33'
			updatedClient2.activity = 'updatedActivity'
			// Update
			const updateResult2 = await sqlClientUpdate.run(updatedClient2)
			expect(updateResult2).toMatchObject(statusOk)
			expect(updateResult2.message).toEqual(expect.stringMatching(updatedClient2.id))

			const clientUpdated2 = await sqlConnection.exec(`SELECT * FROM clients WHERE id = '${updatedClient2.id}'`)[0]
			expect(clientUpdated2.domain).toBe(updatedClient2.domain)
			expect(clientUpdated2.closing_time).toBe(updatedClient2.closing_time)
			expect(clientUpdated2.activity).toBe(updatedClient2.activity)


		})

		test('nosql', async () => {
			// Insert base records
			await nosqlConnection.getCollection('clients').insert(fakeClient1)
			await nosqlConnection.getCollection('clients').insert(fakeClient2)

			const updatedClient1 = { ...fakeClient1 }
			updatedClient1.name = 'updated name'
			// Update
			const updateResult1 = await nosqlClientUpdate.run(updatedClient1)
			expect(updateResult1).toMatchObject(statusOk)
			expect(updateResult1.message).toEqual(expect.stringMatching(updatedClient1.id))

			
			const clientUpdated1 = await nosqlConnection.getCollection('clients').find({ id: updatedClient1.id })[0]
			expect(clientUpdated1.name).toBe(updatedClient1.name)

			const updatedClient2 = { ...fakeClient2 }
			updatedClient2.domain = 'updatedDomain'
			updatedClient2.closing_time = '19:33'
			updatedClient2.activity = 'updatedActivity'
			// Update
			const updateResult2 = await nosqlClientUpdate.run(updatedClient2)
			expect(updateResult2).toMatchObject(statusOk)
			expect(updateResult2.message).toEqual(expect.stringMatching(updatedClient2.id))

			const clientUpdated2 = await nosqlConnection.getCollection('clients').find({ id: updatedClient2.id })[0]
			expect(clientUpdated2.domain).toBe(updatedClient2.domain)
			expect(clientUpdated2.closing_time).toBe(updatedClient2.closing_time)
			expect(clientUpdated2.activity).toBe(updatedClient2.activity)
		})
	})

	describe('Use case validations', () => {
		beforeEach(async () => {
			fakeClient1 = { ...client1 }
			fakeClient2 = { ...client2 }
			await resetDatabases()
		})

		describe('throws error for id', () => {
			test('sql', async () => {
				delete fakeClient1.id
				async function createClient(fakeData) {
					await sqlClientUpdate.run(fakeData)
				}
				// Client with no ID
				await expect(createClient(fakeClient1)).rejects.toThrowError('No ID provided')
				await expect(createClient(fakeClient1)).rejects.toThrowError(ClientError)

			})

			test('nosql', async () => {
				delete fakeClient1.id
				async function createClient(fakeData) {
					await nosqlClientUpdate.run(fakeData)
				}
				// Client with no ID
				await expect(createClient(fakeClient1)).rejects.toThrowError('No ID provided')
				await expect(createClient(fakeClient1)).rejects.toThrowError(ClientError)

			})
		})

	})
})