const ClientCreate = require('../../src/use-cases/ClientCreate')
const dbErrorHandler = require('../../src/errors/dbErrorHandler')
// SQL
const SqlDb = require('../../src/interfaces/SQLAdapter')
const sqlConnection = require('../../src/drivers/alasql')
let sqlDb = new SqlDb({ dbConnection: sqlConnection, dbErrorHandler })

// NoSQL
const NoSqlDb = require('../../src/interfaces/NoSQLAdapter')
const nosqlConnection = require('../../src/drivers/lokidb')
let nosqlDb = new NoSqlDb({ dbConnection: nosqlConnection, dbErrorHandler })

// Tools
const IdGenerator = require('../../src/tools/uuid')

// Helpers
const isEmail = require('../../src/helpers/isEmail')
const isTimeFormat = require('../../src/helpers/isTimeFormat')
const isWeb = require('../../src/helpers/isWeb')

const helpers = {
	isEmail,
	isTimeFormat,
	isWeb
}

async function resetDatabases() {
	await sqlConnection.exec('DELETE FROM clients')
	await sqlConnection.exec('DELETE FROM agents')
	nosqlConnection.removeCollection('clients')
	nosqlConnection.removeCollection('agents')
	nosqlConnection.addCollection('clients', {
		indices: ['id'],
		unique: ['name', 'domain']
	})

	nosqlConnection.addCollection('agents', {
		indices: ['id'],
	})
}

sqlClientCreate = new ClientCreate({ db: sqlDb, IdGenerator, helpers })
nosqlClientCreate = new ClientCreate({ db: nosqlDb, IdGenerator, helpers })

// const Client = require('../../src/entities/Client')
const ClientError = require('../../src/errors/ClientError')
const DatabaseError = require('../../src/errors/DatabaseError')

const { client1, client2 } = require('../fake-data/clientData.json')
let fakeClient1, fakeClient2
const statusOk = { status: 'ok' } 

describe('Client Create', () => {
	beforeEach(() => {
		fakeClient1 = { ...client1 }
		fakeClient2 = { ...client2 }
		resetDatabases()
	})

	describe('creates 2 clients', () => {
		test('sql', async () => {
			delete fakeClient1.id
			delete fakeClient2.id

			const sqlResult = await sqlClientCreate.run(fakeClient1)

			expect(sqlResult).toMatchObject(statusOk)
			expect(sqlResult.message).toEqual(expect.stringMatching(fakeClient1.id))

			const inSqlDb = sqlConnection.exec(`SELECT * FROM clients WHERE id = '${fakeClient1.id}'`)
			expect(inSqlDb).toHaveLength(1)

			const sqlResult2 = await sqlClientCreate.run(fakeClient2)

			expect(sqlResult2).toMatchObject(statusOk)
			expect(sqlResult2.message).toEqual(expect.stringMatching(fakeClient2.id))

			const inSqlDb2 = sqlConnection.exec(`SELECT * FROM clients WHERE id = '${fakeClient2.id}'`)
			expect(inSqlDb2).toHaveLength(1)
		})

		test('nosql', async () => {
			delete fakeClient1.id
			delete fakeClient2.id

			const nosqlResult = await nosqlClientCreate.run(fakeClient1)

			expect(nosqlResult).toMatchObject(statusOk)
			expect(nosqlResult.message).toEqual(expect.stringMatching(fakeClient1.id))

			const inNoSqlDb = nosqlConnection.getCollection('clients').find({ id: fakeClient1.id })
			expect(inNoSqlDb).toHaveLength(1)

			const nosqlResult2 = await nosqlClientCreate.run(fakeClient2)

			expect(nosqlResult2).toMatchObject(statusOk)
			expect(nosqlResult2.message).toEqual(expect.stringMatching(fakeClient2.id))

			const inNoSqlDb2 = nosqlConnection.getCollection('clients').find({ id: fakeClient2.id })
			expect(inNoSqlDb2).toHaveLength(1)
		})
	})

	describe('errors creating new client', () => {
		beforeEach(() => {
			fakeClient1 = { ...client1 }
			fakeClient2 = { ...client2 }
			resetDatabases()
		})
	
		describe('duplicated id', () => {
			test('sql', async () => {
				fakeClient2.id = fakeClient1.id
				await sqlClientCreate.run(fakeClient1)
				async function createClient() {
					await sqlClientCreate.run(fakeClient2)
				}
				await expect(createClient).rejects.toThrowError('exists')
				await expect(createClient).rejects.toThrowError(DatabaseError)
			})

			test('nosql', async () => {
				fakeClient2.id = fakeClient1.id
				await nosqlClientCreate.run(fakeClient1)
				async function createClient() {
					await nosqlClientCreate.run(fakeClient2)
				}
				await expect(createClient).rejects.toThrowError('exists')
				await expect(createClient).rejects.toThrowError(DatabaseError)
			})
		})

		describe('duplicated name', () => {
			test('sql', async () => {
				fakeClient2.name = fakeClient1.name
				await sqlClientCreate.run(fakeClient1)
				async function createClient() {
					await sqlClientCreate.run(fakeClient2)
				}
				await expect(createClient).rejects.toThrowError('exists')
				await expect(createClient).rejects.toThrowError(DatabaseError)
			})

			test('nosql', async () => {
				fakeClient2.name = fakeClient1.name
				await nosqlClientCreate.run(fakeClient1)
				async function createClient() {
					await nosqlClientCreate.run(fakeClient2)
				}
				await expect(createClient).rejects.toThrowError('exists')
				await expect(createClient).rejects.toThrowError(DatabaseError)
			})
		})

		describe('duplicated domain', () => {
			test('sql', async () => {
				fakeClient2.domain = fakeClient1.domain
				await sqlClientCreate.run(fakeClient1)
				async function createClient() {
					await sqlClientCreate.run(fakeClient2)
				}
				await expect(createClient).rejects.toThrowError('exists')
				await expect(createClient).rejects.toThrowError(DatabaseError)
			})

			test('nosql', async () => {
				fakeClient2.domain = fakeClient1.domain
				await nosqlClientCreate.run(fakeClient1)
				async function createClient() {
					await nosqlClientCreate.run(fakeClient2)
				}
				await expect(createClient).rejects.toThrowError('exists')
				await expect(createClient).rejects.toThrowError(DatabaseError)
			})
		})

	})

	describe('Use case validations', () => {
		it('throws error for name', async () => {
			delete fakeClient1.name
			async function createClient() {
				await sqlClientCreate.run(fakeClient1)
			}
		
			await expect(createClient).rejects.toThrowError('Name');
			await expect(createClient).rejects.toThrowError(ClientError);
		})
	
		it('throws error for domain', async () => {
			delete fakeClient1.domain
			async function createClient() {
				await sqlClientCreate.run(fakeClient1)
			}
		
			await expect(createClient).rejects.toThrowError('Domain');
			await expect(createClient).rejects.toThrowError(ClientError);
		})
	
		it('throws error for opening time', async () => {
			delete fakeClient1.opening_time
			async function createClient() {
				await sqlClientCreate.run(fakeClient1)
			}
		
			await expect(createClient).rejects.toThrowError('Opening');
			await expect(createClient).rejects.toThrowError(ClientError);
		})
	
		it('throws error for closing time', async () => {
			delete fakeClient1.closing_time
			async function createClient() {
				await sqlClientCreate.run(fakeClient1)
			}
		
			await expect(createClient).rejects.toThrowError('Closing');
			await expect(createClient).rejects.toThrowError(ClientError);
		})
	
		it('throws error for web', async () => {
			fakeClient1.web = 'notAWebFormat.'
			async function createClient() {
				await sqlClientCreate.run(fakeClient1)
			}
		
			await expect(createClient).rejects.toThrowError('web');
			await expect(createClient).rejects.toThrowError(ClientError);
		})
	
		it('throws error for email', async () => {
			fakeClient1.email = 'notacorrect@email'
			async function createClient() {
				await sqlClientCreate.run(fakeClient1)
			}
		
			await expect(createClient).rejects.toThrowError('email');
			await expect(createClient).rejects.toThrowError(ClientError);
		})
	
		it('throws error invalid time format', async () => {
			fakeClient1.opening_time = '900'
			async function createClient() {
				await sqlClientCreate.run(fakeClient1)
			}
		
			await expect(createClient).rejects.toThrowError('time');
			await expect(createClient).rejects.toThrowError(ClientError);
		})
	
		it('throws error invalid time format', async () => {
			fakeClient1.closing_time = '1900'
			async function createClient() {
				await sqlClientCreate.run(fakeClient1)
			}
		
			await expect(createClient).rejects.toThrowError('time');
			await expect(createClient).rejects.toThrowError(ClientError);
		})
	})

	

})