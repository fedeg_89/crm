const axios = require('axios')
const dotenv = require('dotenv').config({ path: './test/.env.test' })

axios.defaults.baseURL = process.env.BASE_URL + process.env.PORT + '/clients'
axios.defaults.headers.common['Content-Type'] = 'application/json'
// axios.defaults.validateStatus = function (status) {
// 	// Throw only if the status code is greater than or equal to 500
// 	return status < 500
// }

const app = require('../../src/server/server')
const PORT = process.env.PORT
app.listen(PORT, () => {
	console.log(`TESTING - Clients Service listening on port ${PORT}`)
})

const { clientGet } = require('../../src/use-cases/useCases')
const { client1, client2 } = require('../fake-data/clientData.json')
let fakeClient1, fakeClient2
const statusOk = { status: 'ok' } 


describe('CLIENTS SERVICE', () => {
	beforeEach(() => {
		fakeClient1 = { ...client1 }
		fakeClient2 = { ...client2 }
	})

	it('create 2 clients', async () => {
		const response1 = await axios.post(
			'/',
			fakeClient1
		)
		expect(response1.status).toBe(201)
		expect(response1.data).toMatchObject(statusOk)
		expect(response1.data.message).toEqual(expect.stringMatching(fakeClient1.id))
		const get1 = await clientGet.run(fakeClient1.id)
		const doc1 = get1.message[0]
		expect(doc1).toEqual(fakeClient1)

		const response2 = await axios.post(
			'/',
			fakeClient2
		)
		expect(response2.status).toBe(201)
		expect(response2.data).toMatchObject(statusOk)
		expect(response2.data.message).toEqual(expect.stringMatching(fakeClient2.id))
		const get2 = await clientGet.run(fakeClient2.id)
		const doc2 = get2.message[0]
		expect(doc2).toEqual(fakeClient2)
	})
	
	it('get a client', async () => {
		const response= await axios.get(`/${fakeClient1.id}`)
		expect(response.status).toBe(200)
		expect(response.data).toMatchObject(statusOk)
		expect(response.data.message[0]).toEqual(fakeClient1)
	})
	
	it('get all clients', async () => {
		const response= await axios.get(`/`)
		expect(response.status).toBe(200)
		expect(response.data).toMatchObject(statusOk)
		expect(response.data.message).toHaveLength(4)
	})
})