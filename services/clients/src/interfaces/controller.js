const { clientGet, clientCreate, clientUpdate, clientDelete } = require('../use-cases/useCases')

const clientGetApi = (req, res, next) =>
	clientGet.run(req.params.id)
		.then(result =>res.status(200).send(result))
		.catch(next)

const clientCreateApi = (req, res, next) =>
	clientCreate.run(req.body)
		.then(result =>res.status(201).send(result))
		.catch(next)

const clientUpdateApi = (req, res, next) =>
	clientUpdate.run(req.body)
		.then(result =>res.status(201).send(result))
		.catch(next)

const clientDeleteApi = (req, res, next) =>
	clientDelete.run(req.body)
		.then(result =>res.status(201).send(result))
		.catch(next)


const notFound = (req, res) =>
  res.status(404).send({
		statusCode: 400,
		message: 'Not found'
	})
module.exports = { clientGetApi, clientCreateApi, clientUpdateApi, clientDeleteApi, notFound }