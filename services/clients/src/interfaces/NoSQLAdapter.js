class NoSQLAdapter {
	constructor({ dbConnection, dbErrorHandler }) {
		this.dbConnection = dbConnection
		this.dbErrorHandler = dbErrorHandler
		this.collection = 'clients'
	}
	async create(newClient) {
		try {
			await this.validateData(newClient)
			await this.dbConnection.getCollection(this.collection).insert(newClient)
			return true
		} catch (error) {
			this.dbErrorHandler(error.message)
		}
	}

	async update(clientData) {
		try {
			// await this.validateData(clientData)
			const collection = await this.dbConnection.getCollection(this.collection)
			let clientToUpdate = await collection.find({ id: clientData.id })[0]
			
			if (!clientToUpdate) {
				throw new Error('not found')
			} else {
				clientToUpdate = {...clientToUpdate, ...clientData}
				collection.update(clientToUpdate)
				return true
			} 
		} catch (error) {
			console.log('ERROR: ', error)
			this.dbErrorHandler(error.message)
		}
	}

	async delete(client_id) {
		try {
			// Find the record
			const clientToDelete = await this.dbConnection.getCollection(this.collection).find({ id: client_id })[0]
			if (!clientToDelete) throw new Error('not found')

			// Delete the record
			await this.dbConnection.getCollection(this.collection).remove(clientToDelete)
			const result = await this.dbConnection.getCollection(this.collection).find({ id: client_id })

			if (result[0]) {
				throw new Error('unknwon')
			} else {
				return true
			} 
		} catch (error) {
			this.dbErrorHandler(error.message)
		}
	}

	async get(client_id) {
		let result
		try {
			if (client_id) {
				result = await this.dbConnection.getCollection(this.collection).find({ id: client_id })
				if (!result[0]) throw new Error('not found')
			} else {
				result = await this.dbConnection.getCollection(this.collection).find()
			}
			return result
		} catch (error) {
			this.dbErrorHandler(error.message)
		}
	}

	// ++++++++++ //
	async validateData({ id, name, domain }) {
		let {errId, errName, errDomain} = []
		if (id) errId = await this.dbConnection.getCollection(this.collection).find({ id }) 
		if (name) errName = await this.dbConnection.getCollection(this.collection).find({ name })
		if (domain) errDomain = await this.dbConnection.getCollection(this.collection).find({ domain })

		if (errId[0] || errName[0] || errDomain[0]) throw new Error('unique')
	}
}

function updateClient(oldVersion, newVersion) {
	for (const key in newVersion) {
		if (newVersion.hasOwnProperty(key)) {
			oldVersion[key] = newVersion[key]
		}
	}
	return oldVersion
}

module.exports = NoSQLAdapter