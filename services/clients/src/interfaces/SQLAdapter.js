class SQLAdapter {
	constructor({ dbConnection, dbErrorHandler }) {
		this.dbConnection = dbConnection
		this.dbErrorHandler = dbErrorHandler
		this.table = 'clients'
	}
	async create(newClient) {
		try {
			await this.dbConnection.exec(`INSERT INTO ${this.table} VALUES ?`, [newClient])
			return true
		} catch (error) {
			this.dbErrorHandler(error.message)
		}
	}

	async update(clientData) {
		try {
			// Get old data
			const oldClient = await this.dbConnection.exec(`SELECT * FROM ${this.table} WHERE id = '${clientData.id}'`)[0]
			if (!oldClient) throw new Error('not found')

			// Iniciate transaction
			this.dbConnection.exec('BEGIN TRANSACTION')
			try {
				for (const key in clientData) {
					if (clientData.hasOwnProperty(key)) {
						if (key === 'id') continue
						await this.dbConnection.exec(`UPDATE ${this.table} SET ${key} = '${clientData[key]}' WHERE id = '${clientData.id}'`)
					}
				}
				this.dbConnection.exec('COMMIT TRANSACTION')
				return true
			} catch (error) {
				this.dbConnection.exec('ROLLBACK TRANSACTION')
				console.log('ROLLBACK: ', error)
				throw error
			}
			return 
		} catch (error) {
			this.dbErrorHandler(error.message)
		}
	}

	async delete(client_id) {
		try {
			const result = await this.dbConnection.exec(`DELETE FROM ${this.table} WHERE id = '${client_id}'`)
			if (result === 0) throw new Error('unknwon')
			return true
		} catch (error) {
			this.dbErrorHandler(error.message)
		}
	}

	async get(client_id) {
		let result
		try {
			if (client_id) {
				result = await this.dbConnection.exec(`SELECT * FROM ${this.table} WHERE id = '${client_id}'`)
				if (!result[0]) throw new Error('not found')
			} else {
				result = await this.dbConnection.exec(`SELECT * FROM ${this.table}`)
			}
			return result
		} catch (error) {
			this.dbErrorHandler(error.message)
		}
	}
}

function errorHandler(errMsg) {
	if (errMsg.includes('unique')) throw new DatabaseError('The record already exists', 400)
	if (errMsg.includes('already exists')) throw new DatabaseError('The record already exists', 400)
	if (errMsg.includes('NULL')) throw new DatabaseError('There are some fields incomplete', 400)
	if (errMsg.includes('primary key index')) throw new DatabaseError('Record not found', 400)
	if (errMsg.includes('not found')) throw new DatabaseError('Record not found', 400)
	if (errMsg.includes('unknwon')) throw new DatabaseError('Could not delete the record', 400)

	throw new DatabaseError(errorHandler(errMsg), 400)
	// return errMsg
}

module.exports = SQLAdapter