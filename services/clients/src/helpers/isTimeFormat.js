module.exports = isTimeFormat = time => {
	const urlRegex = /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/
	// //var regex = new RegExp(expression);

	if (!time.match(urlRegex)) return false
	
	return true
}