class Base {
	constructor() {

	}

	async run(data) {
		const validData = await this.validate(data)
		return await this.execute(validData)
	}
}

module.exports = Base