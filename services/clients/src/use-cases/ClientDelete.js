const Base = require('./Base')
const Client = require('../entities/Client')
const ClientError = require('../errors/ClientError')

class ClientDelete extends Base {
	constructor({ db }) {
		super()
		this.db = db
	}
	validate(data) {
		if (!data) throw new ClientError('Neither ID or Client provided', 401)
		if (typeof data === 'object' && !data.id) throw new ClientError('No ID provided', 401)

		if (data.id) return data.id

		return data
	}
	async execute(client_id) {
		await this.db.delete(client_id)
		return { status: 'ok', message: `Client ${client_id} deleted` }
	}
	
}

module.exports = ClientDelete