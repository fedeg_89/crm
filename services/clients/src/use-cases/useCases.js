// Use cases
const ClientGet = require('./ClientGet')
const ClientCreate = require('./ClientCreate')
const ClientUpdate = require('./ClientUpdate')
const ClientDelete = require('./ClientDelete')

// Db error handler
const dbErrorHandler = require('../errors/dbErrorHandler')

// Db
const SqlDb = require('../interfaces/SQLAdapter')
const NoSqlDb = require('../interfaces/NoSQLAdapter')

const dbConnection = require('../drivers/alasql')
const db = new SqlDb({ dbConnection , dbErrorHandler })

// Tools
const IdGenerator = require('../tools/uuid')

// Helpers
const isEmail = require('../helpers/isEmail')
const isTimeFormat = require('../helpers/isTimeFormat')
const isWeb = require('../helpers/isWeb')

const helpers = {
	isEmail,
	isTimeFormat,
	isWeb
}

const clientGet = new ClientGet({ db })
const clientCreate = new ClientCreate({ db, IdGenerator, helpers })
const clientUpdate = new ClientUpdate({ db, helpers })
const clientDelete = new ClientDelete({ db })

module.exports = { clientGet, clientCreate, clientUpdate, clientDelete }