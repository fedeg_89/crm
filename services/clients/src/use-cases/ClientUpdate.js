const Base = require('./Base')
const Client = require('../entities/Client')
const ClientError = require('../errors/ClientError')

class ClientUpdate extends Base {
	constructor({ db, helpers }) {
		super()
		this.db = db
		this.helpers = helpers
	}
	validate(data) {
		if (!data.id) throw new ClientError('No ID provided', 401)

		if (data.web && !this.helpers.isWeb(data.web)) throw new ClientError('Invalid web URL', 400)

		if (data.email && !this.helpers.isEmail(data.email)) throw new ClientError('Invalid email', 400)

		if (data.opening_time && !this.helpers.isTimeFormat(data.opening_time)) throw new ClientError('Invalid time format for opening time', 400)
		if (data.closing_time && !this.helpers.isTimeFormat(data.closing_time)) throw new ClientError('Invalid time format for closing time', 400)

		return new Client(data)
	}
	async execute(clientData) {
		const	updatedConfirmation = await this.db.update(clientData)
		return { status: 'ok', message: `Client ${clientData.id} updated` }
	}
	
}

module.exports = ClientUpdate