const Base = require('./Base')
const Client = require('../entities/Client')
const ClientError = require('../errors/ClientError')

class ClientCreate extends Base {
	constructor({ db, IdGenerator, helpers }) {
		super()
		this.db = db
		this.IdGenerator = IdGenerator
		this.helpers = helpers
	}
	validate(data) {
		if (!data.id)	data.id = this.IdGenerator.newId()

		if (!data.name) throw new ClientError('Name is required', 401)
		if (!data.domain) throw new ClientError('Domain is required', 401)
		if (!data.opening_time) throw new ClientError('Opening time is required', 401)
		if (!data.closing_time) throw new ClientError('Closing time is required', 401)

		if (!this.helpers.isWeb(data.web)) throw new ClientError('Invalid web URL', 400)

		if (!this.helpers.isEmail(data.email)) throw new ClientError('Invalid email', 400)

		if (!this.helpers.isTimeFormat(data.opening_time)) throw new ClientError('Invalid time format for opening time', 400)
		if (!this.helpers.isTimeFormat(data.closing_time)) throw new ClientError('Invalid time format for closing time', 400)

		return new Client(data)
		
	}
	async execute(clientData) {
		await this.db.create(clientData)
		return { status: 'ok', message: `Client ${clientData.id} created` }
	}
	
}

module.exports = ClientCreate