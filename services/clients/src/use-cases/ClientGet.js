const Base = require('./Base')
const Client = require('../entities/Client')
const ClientError = require('../errors/ClientError')

class ClientGet extends Base {
	constructor({ db }) {
		super()
		this.db = db
	}
	validate(client_id) {
		if (client_id && typeof client_id !== 'string') throw new ClientError('Invalid ID', 400)
		return client_id
	}
	async execute(client_id) {
		const	clientList = await this.db.get(client_id)
		return { status: 'ok', message: clientList }
	}
	
}

module.exports = ClientGet