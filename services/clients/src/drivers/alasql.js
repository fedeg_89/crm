const alasql = require('alasql')
const IdGenerator = require('../tools/uuid')
const alasqlDb = new alasql.Database('clients')

const loadInitialData = require('../../db/dump-data/alasql/sqlDumpData')({ IdGenerator })
loadInitialData({ db: alasqlDb })

// var clients = alasqlDb.exec("SELECT * FROM clients")
// var agents = alasqlDb.exec("SELECT * FROM agents")

// console.log('clients:', clients)
// console.log('agents: ', agents)

// const update = alasqlDb.exec(`UPDATE clients SET name = 'updated name' WHERE id = '${clients[0].id}'`)
// console.log(update)
// clients = alasqlDb.exec("SELECT * FROM clients")
// console.log('clients:', clients)
module.exports = alasqlDb