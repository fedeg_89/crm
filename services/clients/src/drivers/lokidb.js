const loki = require('lokijs')
const lokiDb = new loki('clients')

const loadInitialData = require('../../db/dump-data/lokidb/nosqlDumpData')()
loadInitialData({ db: lokiDb })

module.exports = lokiDb