class Client {
	constructor({ id, name, web, activity, domain, address, tel, email, opening_time, closing_time }) {
		this.id = id
		this.name = name
		this.web = web
		this.activity = activity
		this.domain = domain
		this.address = address
		this.tel = tel
		this.email = email
		this.opening_time = opening_time
		this.closing_time = closing_time
	}
}
module.exports = Client