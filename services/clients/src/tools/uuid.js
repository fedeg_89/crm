const uuid = require('uuid')
const IdError = require('../errors/IdError')

class IdGenerator {
	constructor() {	}

	newId(name, namespace) {
		if ((!name && namespace) || (name && !namespace)) throw new IdError("Both 'name' and 'namespace' must be provided.")
		
		if (!name && !namespace) {
			return uuid.v4()
		} else {
			return uuid.v5(name, namespace)
		}
	}
}

module.exports = new IdGenerator()