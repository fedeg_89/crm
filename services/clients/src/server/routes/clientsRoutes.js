const express = require('express')
const clientsRoutes = express.Router()

const {	clientGetApi, clientCreateApi, clientUpdateApi, clientDeleteApi } =  require('../../interfaces/controller')

clientsRoutes.route('/:id')
	.get(clientGetApi)

clientsRoutes.route('/')
	.get(clientGetApi)
	.post(clientCreateApi)
	.patch(clientUpdateApi)
	.delete(clientDeleteApi)


module.exports = clientsRoutes
