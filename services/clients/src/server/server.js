const express = require('express')

const {	notFound } =  require('../interfaces/controller')

const errorResponse = require('./routes/errorResponse')

const app = express()
const cors = require('cors')
app.use(cors())
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

const clientsRoutes = require('./routes/clientsRoutes')
app.use('/clients', clientsRoutes)

// Catch and send error messages
app.use(errorResponse)

// Not Found
app.use(notFound)



module.exports = app