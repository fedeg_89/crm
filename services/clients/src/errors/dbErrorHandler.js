const DatabaseError = require('./DatabaseError')

module.exports = errorHandler = (errMsg) => {
	if (errMsg.includes('unique')) throw new DatabaseError('The record already exists', 400)
	if (errMsg.includes('already exists')) throw new DatabaseError('The record already exists', 400)
	if (errMsg.includes('NULL')) throw new DatabaseError('There are some fields incomplete', 400)
	if (errMsg.includes('primary key index')) throw new DatabaseError('Record not found', 400)
	if (errMsg.includes('not found')) throw new DatabaseError('Record not found', 400)
	if (errMsg.includes('not defined')) throw new DatabaseError('Wrong field sent', 400)
	if (errMsg.includes('unknwon')) throw new DatabaseError('Could not delete the record', 400)

	throw new DatabaseError(errMsg, 400)
	// return errMsg
}

