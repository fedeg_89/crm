class DatabaseError extends Error {  
  constructor (message, status) {
    super(message)

    this.name = this.constructor.name
    this.statusCode = status
  }

  getStatusCode() {
    return this.status
	}

}

module.exports = DatabaseError  