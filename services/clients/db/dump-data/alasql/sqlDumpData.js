let name, clientId, agentId
function loadInitialData ({ IdGenerator }) {
	return function fillDatabase({ db }) {
		db.exec(`CREATE TABLE clients (
			id varchar(36) NOT NULL PRIMARY KEY,
			name varchar(30) NOT NULL UNIQUE,
			web varchar(50),
			activity varchar(15),
			domain varchar(15) NOT NULL UNIQUE,
			address varchar(50),
			tel varchar(20),
			email varchar(50),
			opening_time time NOT NULL,
			closing_time time NOT NULL
		)`)

		db.exec(`CREATE TABLE agents (
			id varchar(36) NOT NULL PRIMARY KEY,
			client_id varchar(36) NOT NULL FOREIGN KEY REFERENCES clients(id),
			name varchar(30) NOT NULL,
			tel varchar(20),
			mail varchar(50) NOT NULL,
			charge varchar(15)
		)`)

		name = 'Decoration Store'
		clientId = IdGenerator.newId()
		db.exec("INSERT INTO clients VALUES ( " +
			`"${clientId}",` +
			`"${name}",` +
			'"www.decostore.com", ' +
			'"decoration", ' +
			'"decorationstore", ' +
			'"some street 1234", ' +
			'"000-1234-1232", ' + 
			'"contact@decostore.com", ' +
			'"09:00", ' +
			'"19:00"' +
		")")

		name = 'Jane Gale'
		agentId = IdGenerator.newId(name, clientId)
		db.exec("INSERT INTO agents VALUES ( " +
			`"${agentId}",` +
			`"${clientId}",` +
			`"${name}",` +
			'"000-1234-1232",' +
			'"janegale@decostore.com",' +
			'"manager"' +
		")")

		name = 'Super Market'
		clientId = IdGenerator.newId()
		db.exec("INSERT INTO clients VALUES ( " +
			`"${clientId}",` +
			`"${name}",` +
			'"www.supermarket.com", ' +
			'"groceries", ' +
			'"supermarket", ' +
			'"mighty avenue 5432", ' +
			'"000-4444-9876", ' + 
			'"info@supermarket.com", ' +
			'"08:00", ' +
			'"22:00"' +
		")")

		name = 'Joan Amaze'
		agentId = IdGenerator.newId(name, clientId)
		db.exec("INSERT INTO agents VALUES ( " +
			`"${agentId}",` +
			`"${clientId}",` +
			`"${name}",` +
			'"000-1122-3344",' +
			'"jamaze@supermarket.com",' +
			'"manager"' +
		")")

		name = 'Henry Valery'
		agentId = IdGenerator.newId(name, clientId)
		db.exec("INSERT INTO agents VALUES ( " +
			`"${agentId}",` +
			`"${clientId}",` +
			`"${name}",` +
			'"000-4455-5432",' +
			'"hvalery@supermarket.com",' +
			'"coordinator"' +
		")")

	}
}

module.exports = loadInitialData