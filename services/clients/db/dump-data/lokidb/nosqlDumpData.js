let name, clientId, agentId
function loadInitialData () {
	return function fillDatabase({ db }) {
		const clients = db.addCollection('clients', {
			indices: ['id'],
			unique: ['name', 'domain']
		})

		const agents = db.addCollection('agents', {
			indices: ['id'],
		})

		clients.insert({
			id: '51fc95d4-b96c-4394-85bd-a1c26c0bfdbe',
			name: 'Decoration Store',
			web: 'www.decostore.com',
			activity: 'decoration',
			domain: 'decorationstore',
			address: 'some street 1234',
			tel: '000-1234-1232',
			email: 'contact@decostore.com',
			opening_time: '09:00',
			closing_time: '19:00'
		})
			
		agents.insert({
			id: 'a87a5145-5d01-479f-96d0-f7d343f482ca',
			client_id: '51fc95d4-b96c-4394-85bd-a1c26c0bfdbe',
			name: 'Jane Gale',
			tel: '000-1234-1232',
			mail: 'janegale@decostore.com',
			charge: 'manager'
		})

		clients.insert({
			id: 'b12edf4b-8b8a-4fec-b532-34ff7157a6b1',
			name: 'Super Market',
			web: 'www.supermarket.com',
			activity: 'groceries',
			domain: 'supermarket',
			address: 'mighty avenue 5432',
			tel: '000-4444-9876',
			email: 'info@supermarket.com',
			opening_time: '08:00',
			closing_time: '22:00'
		})
		
		agents.insert({
			id: '5ac40dd7-2dcb-4ae6-b179-a83f2e7803a1',
			client_id: 'b12edf4b-8b8a-4fec-b532-34ff7157a6b1',
			name: 'Joan Amaze',
			tel: '000-1122-3344',
			mail: 'jamaze@supermarket.com',
			charge: 'manager'
		})

		agents.insert({
			id: '09ad5919-c687-4d90-9afc-8775863ba9fe',
			client_id: 'b12edf4b-8b8a-4fec-b532-34ff7157a6b1',
			name: 'Henry Valery',
			tel: '000-4455-5432',
			mail: 'hvalery@supermarket.com',
			charge: 'coordinator'
		})

	}
}

module.exports = loadInitialData